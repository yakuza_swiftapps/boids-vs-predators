﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkHandler : Photon.PunBehaviour {
	private const string gameVersion = "1";

	public delegate void LeftRoom();
    public event LeftRoom LeftRoomEvent;

	public delegate void PlayerConnected(PhotonPlayer other);
    public event PlayerConnected PlayerConnectedEvent;
	
	public delegate void PlayerDisconnected(PhotonPlayer other);
    public event PlayerDisconnected PlayerDisconnectedEvent;
	
	public delegate void RandomJoinFailed(object[] codeAndMsg);
    public event RandomJoinFailed RandomJoinFailedEvent;
	
	public delegate void PhotonJoinRoomFailed(object[] codeAndMsg);
    public event PhotonJoinRoomFailed PhotonJoinRoomFailedEvent;
	
	public delegate void CreateRoomFailed(object[] codeAndMsg);
    public event CreateRoomFailed CreateRoomFailedEvent;
	
	public delegate void JoinedRoom();
    public event JoinedRoom JoinedRoomEvent;
	
	public delegate void Connected();
    public event Connected ConnectedEvent;
	
	public delegate void FailedToConnect();
    public event FailedToConnect FailedToConnectEvent;
	
	public delegate void PropertiesChanged(ExitGames.Client.Photon.Hashtable changed);
    public event PropertiesChanged PropertiesChangedEvent;

	void Awake() {
		if (!PlayerPrefs.HasKey("GUID")) PlayerPrefs.SetString("GUID", System.Guid.NewGuid().ToString());
		if (!PhotonNetwork.connected) {
			PhotonNetwork.ConnectUsingSettings(gameVersion);
			ExitGames.Client.Photon.PhotonPeer.RegisterType(typeof(Scoreboard.PlayerEntry), 255, Scoreboard.PlayerEntry.Serialize, Scoreboard.PlayerEntry.Deserialize);
		}
	}

	void OnJoinedRoom () {
		if (JoinedRoomEvent != null) JoinedRoomEvent();
	}
	public void OnLeftRoom() {
		LeftRoomEvent();
	}

	void OnPhotonPlayerConnected(PhotonPlayer other) {
		PlayerConnectedEvent(other);
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer other) {
		PlayerDisconnectedEvent(other);
	}
	
	void OnPhotonRandomJoinFailed (object[] codeAndMsg) {
		RandomJoinFailedEvent(codeAndMsg);
	}
	
	void OnPhotonJoinRoomFailed(object[] codeAndMsg) {
		PhotonJoinRoomFailedEvent(codeAndMsg);
	}
	
	override public void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable changed) {
		if (PropertiesChangedEvent != null) PropertiesChangedEvent(changed);
	}

	void OnPhotonCreateRoomFailed (object[] codeAndMsg) {
		CreateRoomFailedEvent(codeAndMsg);
	}

	void OnConnectedToMaster() {
		Debug.Log("Połączono z siecią.");
		// ConnectedEvent();
	}

	void OnDisconnectedFromPhoton() {
		Debug.LogError("Coś poszło nie tak. Łączę ponownie...");
		PhotonNetwork.ConnectUsingSettings(gameVersion);
	}

	void OnFailedToConnectToPhoton(DisconnectCause cause) {
		FailedToConnectEvent();
	}

	public void CreateRoom(string name) {
		PhotonNetwork.CreateRoom(name, new RoomOptions() { MaxPlayers = 255 }, null);
	}

	public void JoinRoom(string name) {
		PhotonNetwork.JoinRoom(name);
	}

	public void RPC(string methodName, PhotonTargets target, params object[] parameters) {
		Debug.Log("RPC " + methodName + " " + target + " " + parameters);
		PhotonView.Get(this).RPC(methodName, target, parameters);
	}

	[PunRPC]
	void DestroyObjectRPC(int id) {
		PhotonNetwork.Destroy(PhotonView.Find(id).gameObject);
	}
}
