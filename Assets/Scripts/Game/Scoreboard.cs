﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Scoreboard : MonoBehaviour {
	[Inject] private SignalBus signalBus;
	[Inject] private NetworkHandler network;

	private Hashtable players = new Hashtable();
	private Text label;
	
	void Awake () {
		label = GetComponent<Text>();
		network.PropertiesChangedEvent += mergeData;
	}
	
	public void FetchData() {
		foreach (DictionaryEntry pair in PhotonNetwork.room.CustomProperties) players[pair.Key] = pair.Value;
		updatePlayerList();
	}

	private void mergeData(ExitGames.Client.Photon.Hashtable newData) {
		foreach (DictionaryEntry pair in newData) players[pair.Key] = pair.Value;
		updatePlayerList();
	}

	private void updatePlayerList() {
		var builder = "";

		var iCounter = players.Count;
		foreach (DictionaryEntry pair in players) {
			iCounter--;
			if ("curScn".Equals(pair.Key) || pair.Value == null) continue;

			builder += pair.Key;
			var data = pair.Value as PlayerEntry;
			if (data.isPlaying) builder += " (w grze)";
			else builder += "				Najlepszy wynik: " + data.maxScore;

			if (iCounter > 0) builder += "\n";
		}
		label.text = builder;
	}
	
	public class PlayerEntry {
		public int maxScore;
		// public int currentScore;
		public bool isPlaying;

		public static object Deserialize(byte[] data) {
			var result = new PlayerEntry();
			result.maxScore = data[0] + data[1] * 256;
			// result.currentScore = data[1] + data[2] * 256;
			result.isPlaying = (data[2] == 1);
			return result;
		}

		public static byte[] Serialize(object customType) {
			var e = (PlayerEntry)customType;
			return new byte[] {(byte)(e.maxScore), (byte)(e.maxScore/256), /*(byte)(e.currentScore), (byte)(e.currentScore/256),*/ e.isPlaying ? (byte)1 : (byte)0};
		}
	}
}
