//
//  TDProperties.h
//  Tapdaq
//
//  Created by Tapdaq <support@tapdaq.com>
//  Copyright (c) 2016 Tapdaq. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TDOrientationEnum.h"
#import "TDAdTypeEnum.h"
#import "TDPlacement.h"
#import "TDTestDevices.h"
#import "TDLogLevel.h"


typedef NS_ENUM(NSInteger, TDSubjectToGDPR) {
    TDSubjectToGDPRNo = 0,
    TDSubjectToGDPRYes = 1,
    TDSubjectToGDPRUnknown = 2
};


@interface TDProperties : NSObject

/**
 * Enables/Disables logging of the adapters.
 */
@property (nonatomic) BOOL isDebugEnabled;

/**
 * Enables/Disables ad auto reloading.
 */
@property (nonatomic) BOOL autoReloadAds;

/**
 Note: For plugin developers only.
 */
@property (nonatomic) NSString *pluginVersion;

/**
 * Set how fine-grained information is provided in Tapdaq's logs.
 * Default level is TDLogLevelInfo
 */
@property (assign, nonatomic) TDLogLevel logLevel;

/**
 * User is subject to EU GDPR laws.
 */
@property (assign, nonatomic) TDSubjectToGDPR userSubjectToGDPR;

/**
 * Property that indicates whether a user has given consent to use his private data.
 */
@property (assign, nonatomic) BOOL isConsentGiven;

/**
 * User is of age 16 or below.
 */
@property (assign, nonatomic) BOOL isAgeRestrictedUser;

/**
 * An new instance with default values;
 */
+ (instancetype)defaultProperties;

/**
 To use placement tags, you must create a TDPlacement object and register it.
 If you do not register a placement tag but attempt to use a custom one elsewhere, adverts will not display.
 
 @param placement The TDPlacement object to be registered
 */
- (BOOL)registerPlacement:(TDPlacement *)placement;

/**
 To use placement tags, you must create a TDPlacement object and register it.
 If you do not register a placement tag but attempt to use a custom one elsewhere, adverts will not display.
 
 @param placements An NSArray of TDPlacement objects to be registered
 */
- (BOOL)registerPlacements:(NSArray *)placements;

- (BOOL)registerTestDevices:(TDTestDevices *)testDevices;

- (NSArray *)registeredPlacements;

- (NSArray *)registeredTestDevices;

@end
