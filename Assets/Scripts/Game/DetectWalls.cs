﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectWalls : MonoBehaviour {
	private Boid boid;

	void Start () {
		boid = GetComponentInParent<Boid>();
	}
	
	void OnTriggerEnter(Collider wall) {
		boid.AddWall(wall.gameObject);
	}
	
	void OnTriggerExit(Collider wall) {
		boid.RemoveWall(wall.gameObject);
	}
}
