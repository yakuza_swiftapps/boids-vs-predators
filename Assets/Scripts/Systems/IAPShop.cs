﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using Zenject;

public class IAPShop : IInitializable, IStoreListener {
	[Inject] PlayerStats playerStats;
	[Inject] SignalBus signalBus;

	private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

	public const string product1ID = "boids.inapp1";
	public const string product2ID = "boids.inapp2";
	
	public void Initialize () {
		if (m_StoreController == null) {
			InitializePurchasing();
		}
	}

	public void InitializePurchasing() {
            if (IsInitialized()) {
                return;
            }
            
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            builder.AddProduct(product1ID, ProductType.Consumable);
            builder.AddProduct(product2ID, ProductType.Consumable);
            UnityPurchasing.Initialize(this, builder);
	}

	public bool IsInitialized() {
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
		signalBus.Fire(new IAPInitialized());
	}

	public void OnInitializeFailed(InitializationFailureReason error) {
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}

	public void BuyProductID(string productId) {
            if (IsInitialized()) {
                Product product = m_StoreController.products.WithID(productId);
                
				if (product != null && product.availableToPurchase) {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}' / {1}", product.definition.id, product.metadata.localizedTitle));
                    m_StoreController.InitiatePurchase(product);
                } else {
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            } else {
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
		bool validPurchase = true;
		
		#if !UNITY_EDITOR && !UNITY_STANDALONE
		try {
			var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
			var result = validator.Validate(args.purchasedProduct.receipt);

			Debug.Log("Receipt is valid. Contents:");
			foreach (IPurchaseReceipt productReceipt in result) {
				Debug.Log(productReceipt.productID);
				Debug.Log(productReceipt.purchaseDate);
				Debug.Log(productReceipt.transactionID);
			}
		} catch (IAPSecurityException) {
			Debug.Log("Invalid receipt, not unlocking content");
			validPurchase = false;
		}
		#endif

		if (validPurchase) {
			if (args.purchasedProduct.definition.id.Equals(product1ID)) {
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				playerStats.ModifyCoins(100);
				signalBus.Fire(new IAPPurchased());
			} else if (args.purchasedProduct.definition.id.Equals(product2ID)) {
				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				playerStats.ModifyCoins(151);
				signalBus.Fire(new IAPPurchased());
			} else {
				Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
			}
		}

		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}

	public Product GetProduct(string id) {
        return m_StoreController.products.WithID(id);
	}
}
