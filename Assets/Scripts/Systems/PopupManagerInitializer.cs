﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class PopupManagerInitializer : MonoBehaviour {
	[Inject] PopupManager popman;

	void Awake() {
		DontDestroyOnLoad(gameObject);
	}
	
	void Start () {
		popman.panel = transform.GetChild(0).GetComponent<RectTransform>();
		popman.text = GetComponentInChildren<Text>();
		popman.Initialize();
	}
}
