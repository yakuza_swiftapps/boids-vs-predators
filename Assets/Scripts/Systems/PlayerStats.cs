﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Newtonsoft.Json;

public class PlayerStats : ITickable, IInitializable {
	[Inject] SignalBus signalBus;

	private PlayerData data;

	private FirebaseApp app;
	private DatabaseReference db;

	private const float syncRate = 10f;

	private float lastTick;
    private int loadProgress;

	public void Initialize() {
		signalBus.Subscribe<BoidEatenSignal>(() => data.BoidsEaten++);
		signalBus.Subscribe<NewGameSignal>(() => data.GamesPlayed++);
		signalBus.Subscribe<AddGameTimeSignal>(() => data.TimePlayed += Time.deltaTime);
		signalBus.Subscribe<AchievementGet>((x) => AddAchievement(x.name));

        FirebaseInitializeSDK(PlayerPrefs.GetString("GUID"));
	}

	public void Tick() {
        if (Time.time - lastTick > syncRate && IsInitialized()) {
			Debug.Log("DatabaseSync");
			db.Child("PlayerStats").Child(PlayerPrefs.GetString("GUID")).SetRawJsonValueAsync(JsonConvert.SerializeObject(data));
			lastTick = Time.time;
        } else if (Time.time - lastTick > syncRate) {
			signalBus.Fire(new FireBaseFailSignal() {step = loadProgress});
            Debug.LogWarning(string.Format("Niepowodzenie wczytania Firebase na kroku {0}, ponawiam próbę...", loadProgress));
            lastTick = Time.time;
       		FirebaseInitializeSDK(PlayerPrefs.GetString("GUID"));
        }
    }

	public void ModifyCoins(int amount) {
		data.Coins += amount;
		ForceUpdate();
	}

	public void AddAchievement(string name) {
		if (!data.Achievements.Contains(name)) data.Achievements.Add(name);
		ForceUpdate();
	}

	public List<string> GetAchievements() {return data.Achievements;}

	public int GetCoins() {return data.Coins;}
    private void ForceUpdate() {lastTick = -syncRate;}
    public bool IsInitialized() { return loadProgress >= 5; }
	public int LoadingProgress() {return loadProgress;}

	[System.Serializable]
	public class PlayerData {
		public int GamesPlayed;
		public int BoidsEaten;
        public float TimePlayed;
        public int Coins;
        public List<string> Achievements;
	}

	public void FirebaseInitializeSDK(string guid) {
        if (loadProgress >= 1) { FirebaseAuthenticate(guid); return; }

		FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
			var dependencyStatus = task.Result;
			if (dependencyStatus == Firebase.DependencyStatus.Available) {
                loadProgress++;
				signalBus.Fire(new FireBaseNextStepSignal());
				app = FirebaseApp.DefaultInstance;
				FirebaseAuthenticate(guid);
			} else {
				UnityEngine.Debug.LogError(System.String.Format(
				"Could not resolve all Firebase dependencies: {0}", dependencyStatus));
			}
		});
	}

    private void FirebaseAuthenticate(string guid) {
        if (loadProgress >= 2) { FirebaseLoadRemoteConfig(guid); return; }

		Firebase.Auth.FirebaseAuth.DefaultInstance.SignInAnonymouslyAsync().ContinueWith(task => {
		if (task.IsCanceled) {
			Debug.LogError("SignInAnonymouslyAsync was canceled.");
			return;
		}
		if (task.IsFaulted) {
			Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
			return;
		}

        loadProgress++;
		signalBus.Fire(new FireBaseNextStepSignal());
		Firebase.Auth.FirebaseUser newUser = task.Result;
		Debug.LogFormat("User signed in successfully: {0} ({1})",
			newUser.DisplayName, newUser.UserId);
            FirebaseLoadRemoteConfig(guid);
		});
	}

	private string[] configVariables = new string[] {"boid_speed_randomness", "predator_chasing_speed", "boid_escaping_speed",
	"boid_normal_speed","predator_normal_speed", "predator_control_time", "max_boids"};

    private void FirebaseLoadRemoteConfig(string guid) {
        if (loadProgress >= 3) { FirebaseLoadDatabase(guid); return; }

		var defaults = new Dictionary<string, object>();

		foreach (string s in configVariables)
			defaults.Add(s, PlayerPrefs.GetFloat(s));
		
		Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);

		Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(new System.TimeSpan()).ContinueWith(task => {
            loadProgress++;
			signalBus.Fire(new FireBaseNextStepSignal());
			if (!task.IsCompleted) {
				Debug.LogWarning("Nie udało się wczytać konfiguracji. Używam domyślnych wartości.");
			}
			Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();

			foreach (string s in configVariables)
				PlayerPrefs.SetFloat(s, (float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue(s).DoubleValue);

            FirebaseLoadDatabase(guid);
		});
    }

    private void FirebaseLoadDatabase(string guid) {
        if (loadProgress >= 4) { FetchPlayerData(guid); return; }

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://boids-vs-predators.firebaseio.com/");
        db = FirebaseDatabase.DefaultInstance.RootReference;
        loadProgress++;
		signalBus.Fire(new FireBaseNextStepSignal());
        FetchPlayerData(guid);
    }

    private void FetchPlayerData(string guid)
    {
        //if (loadProgress >= 5) { NextMethodIfEverNeeded(guid); return; }

        db.Child("PlayerStats").Child(guid).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("Nie udało się wczytać danych gracza!");
            }
            else if (task.IsCompleted)
            {
                if (task.Result != null && task.Result.Value != null)
                {
                    var newData = task.Result.Value;
                    data = JsonConvert.DeserializeObject<PlayerData>(JsonConvert.SerializeObject(newData));
					if (data.Achievements == null) data.Achievements = new List<string>();
                }
                else
                    data = new PlayerData();

                loadProgress++;
                signalBus.Fire(new FireBaseInitializedSignal());
                Debug.Log("Gracz załadowany pomyślnie!");
            }
        });
    }
}
