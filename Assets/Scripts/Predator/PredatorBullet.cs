﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorBullet : MonoBehaviour, IPunObservable  {
	[SerializeField] private const float SPEED = 2;

	Rigidbody rb;

	void Start () {
		rb = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate () {
		rb.MovePosition(transform.position + transform.forward * SPEED);
	}

	void OnCollisionEnter(Collision c) {
		if (c.collider.tag == "Map") PhotonNetwork.Destroy(PhotonView.Get(this));
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) {
			stream.SendNext(transform.position.x);
			stream.SendNext(transform.position.z);
		} else {
			transform.position = new Vector3((float)stream.ReceiveNext(), transform.position.y, (float)stream.ReceiveNext());
		}
	}
}