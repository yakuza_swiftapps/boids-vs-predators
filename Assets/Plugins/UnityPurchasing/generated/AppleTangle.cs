#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("hJsqaK2jgK2qrq6sqambKh2xKhiYnfGbyZqgm6KtqP6vrbip/viauNnKyN/CyM6L2N/K387GzsXf2IWbvZu/raj+r6i4purb28fOi/nExN+bKa8QmymoCAuoqaqpqaqpm6atouJz3TSYv84K3D9ihqmoqquqCCmqHLAWOOmPuYFspLYd5jf1yGPgK7zc3IXK29vHzoXIxMaEytvbx87IysLNwsjK38LExYvq3t/DxNnC39Kaxc+LyMTFz8LfwsTF2IvEzYve2M4VX9gwRXnPpGDS5J9zCZVS01TAY8fOi+LFyIWajZuPraj+r6C4turbycfOi9jfysXPytnPi9/O2cbYi8oam/NH8a+ZJ8MYJLZ1zthUzPXOF+7VtOfA+z3qIm/fyaC7KOosmCEqtDpwtez7QK5G9dIvhkCdCfzn/kevrbip/viauJu6raj+r6G4oerb22vImNxckayH/UBxpIqlcRHYsuQez56IvuC+8rYYP1xdNzVk+xFq8/vbx86L+cTE34vo6pu1vKabnZufmaOAraqurqypqr21w9/f29iRhITctC4oLrAyluycWQIw6yWHfxo7uXOLxM2L38POi9/DzsWLytvbx8LIynKd1Gos/nIMMhKZ6VBzfto11Qr5gS3jLVymqqqurqubyZqgm6KtqP6dMueG0xxGJzB3WNwwWd153JvkaoeLyM7Z38LNwsjK386L28THwsjSjZuPraj+r6C4turb28fOi+jO2d+j9ZspqrqtqP62i68pqqObKaqvm5u6raj+r6G4oerb28fOi+LFyIWa0ovK2Njexs7Yi8rIyM7b38rFyM6F6w1c7ObUo/WbtK2o/raIr7ObvdTqAzNSemHNN4/AunsIEE+wgWi0i8rFz4vIztnfws3CyMrfwsTFi9vRmymq3Zulraj+tqSqqlSvr6ipqj410acP7CDwf72cmGBvpOZlv8J6ILIidVLgx16sAImbqUOzlVP7oniPSUB6HNt0pO5KjGFaxtNGTB68vN/CzcLIyt/Oi8nSi8rF0ovbytnf38PE2cLf0pq9m7+tqP6vqLim6tspqqutooEt4y1cyM+uqpsqWZuBraQ2lliA4oOxY1VlHhKlcvW3fWCWnpman5uYnfG8ppiem5mbkpman5utqP62pa+9r7+Ae8LsP92iVV/AJqxH1pIoIPiLeJNvGhQx5KHAVIBXrZukraj+triqqlSvrpuoqqpUm7Yk2CrLbbDwooQ5GVPv41vLkzW+Xq6rqCmqpKubKaqhqSmqqqtPOgKiA3fViZ5hjn5ypH3AfwmPiLpcCgeL6OqbKaqJm6atooEt4y1cpqqqqgAI2jns+P5qBITqGFNQSNtmTQjn28fOi+jO2d/CzcLIyt/CxMWL6t4rv4B7wuw/3aJVX8AmhesNXOzm1Mwkox+LXGAHh4vE2x2UqpsnHOhk+c7HwsrFyM6LxMWL38PC2IvIztkekQZfpKWrOaAair2F336XpnDJvZaNzIshmMFcpilkdUAIhFL4wfDP8gyuote86/26td94HCCIkOwIfsRistle9qV+1PQwWY6oEf4k5vamWqatooEt4y1cpqqqrq6rqCmqqqv3+wEhfnFPV3uirJwb3t6K");
        private static int[] order = new int[] { 37,8,46,12,37,27,59,36,11,43,50,12,42,26,25,15,46,57,20,59,35,49,41,58,42,48,44,50,33,43,40,31,57,45,45,35,46,53,39,50,45,41,46,45,51,45,55,48,54,51,55,53,57,53,55,58,56,57,59,59,60 };
        private static int key = 171;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
