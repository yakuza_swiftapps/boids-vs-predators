﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using Zenject;

public class PredatorAgent : Agent {
	[Inject] private MapSettings mapSettings;

	LayerMask FLOOR;
	private float ABOVE_FLOOR = 0.6f;
	private int MAX_SPAWN_ITERATIONS = 100;
  private Vector4 bounds;
  private float previousDist;

  SmartPredator predator;

  void Start () {
		FLOOR = LayerMask.GetMask(new string[] {"Floor"});
    predator = GetComponent<SmartPredator>();
    bounds = mapSettings.GetBounds();
  }

	RaycastHit CheckFloor(Vector3 raycastOrigin) {
		RaycastHit floor;
		Physics.Raycast(raycastOrigin, Vector3.down, out floor, Camera.main.transform.position.y * 2, FLOOR); //TODO: uważać na ściany (te wewnętrzne)
		return floor;
	}

	public override void CollectObservations() {
    var boid = predator.GetClosestBoid();
    AddVectorObs(predator.BoidCount());

    if (boid) {
      predator.chasing = true;
      var relativePosition = boid.transform.position - transform.position;
      AddVectorObs(relativePosition.x / bounds.z);
      AddVectorObs(relativePosition.z / bounds.w);
      AddVectorObs(boid.transform.rotation.z / Mathf.PI);
    } else {
      predator.chasing = false;
      AddVectorObs(0);
      AddVectorObs(0);
      AddVectorObs(0);
    }
	}
	
	public override void AgentAction(float[] vectorAction, string textAction) {
		predator.ModifyVelocity(new Vector3(vectorAction[0] * 10f, 0, 0), new Vector3(0, 0, vectorAction[1] * 10f), (x, y) => x + y);

    if (predator.succeeded) {
      AddReward(1f);
      Done();
    } else {
      var boid = predator.GetClosestBoid();

      if (boid) {
        var dist = (boid.transform.position - transform.position).sqrMagnitude;
        if (dist < previousDist) AddReward(0.1f);
        previousDist = dist;
      } else {
        AddReward(-0.01f);
      }
    }
	}
}
