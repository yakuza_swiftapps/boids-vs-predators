﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class RoomManager : MonoBehaviour {
	[Inject] private BoidContainer boidContainer;
	[Inject] private SignalBus signalBus;
	[Inject] private NetworkHandler network;
	[Inject] private Chat chat;
	[Inject] private Scoreboard scoreboard;
	[Inject] private Achievements achvs;

	List<PhotonPlayer> players = new List<PhotonPlayer>();

	[SerializeField] private Text roomName;
	[SerializeField] private Button chatButton;
	[SerializeField] private InputField chatInput;

	void Awake() {
		if (!PhotonNetwork.connected) {
			Debug.LogWarning("Nie połączono, tryb offline uruchomiony");
			PhotonNetwork.offlineMode = true;
			PhotonNetwork.CreateRoom("OFFLINE");
			PhotonNetwork.player.NickName = "OFFLINE PLAYER";
		}

		network.PlayerConnectedEvent += PlayerConnected;
		network.PlayerDisconnectedEvent += PlayerDisconnected;
		PhotonNetwork.room.IsVisible = true;
	}

	void Start() {
		ScreenFader.FadeIn();
		roomName.text = "Nazwa pokoju: " + PhotonNetwork.room.Name;

		signalBus.Subscribe<AnalyticsGameStarted>(gameStarted);
		signalBus.Subscribe<AnalyticsGameFinished>((x) => gameFinished(x.boidsEaten));

		chatButton.onClick.AddListener(onChatClick);

		addPlayer(PhotonNetwork.player);
		scoreboard.FetchData();
	}

	private void onChatClick() {
		if (chatInput.text == "") {
			chatInput.Select();
 			chatInput.ActivateInputField();
		} else {
			RPC("ChatRPC", PhotonTargets.All, PhotonNetwork.playerName, chatInput.text);
			chatInput.text = "";
			chatInput.DeactivateInputField();
		}
	}

	private void addPlayer(PhotonPlayer player) {
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.room.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() {{player.NickName, new Scoreboard.PlayerEntry()}});
		}
	}

	private void removePlayer(PhotonPlayer player) {
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.room.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() {{player.NickName, null}});
		}
	}

	private void gameStarted() {
		RPC("GameStartedRPC", PhotonTargets.Others, PhotonNetwork.playerName);
		startPlayer(PhotonNetwork.player.NickName);
	}

	private void startPlayer(string playerName) {
		if (PhotonNetwork.isMasterClient) {
			var playerData = PhotonNetwork.room.CustomProperties[playerName] as Scoreboard.PlayerEntry;
			playerData.isPlaying = true;
			PhotonNetwork.room.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() {{playerName, playerData}});
		}
	}

	private void gameFinished(int boidsEaten) {
		RPC("GameFinishedRPC", PhotonTargets.Others, PhotonNetwork.playerName, boidsEaten);
		finishPlayer(PhotonNetwork.player.NickName, boidsEaten);
	}

	private void finishPlayer(string playerName, int boidsEaten) {
		if (PhotonNetwork.isMasterClient) {
			var playerData = PhotonNetwork.room.CustomProperties[playerName] as Scoreboard.PlayerEntry;
			playerData.isPlaying = false;
			playerData.maxScore = Mathf.Max(playerData.maxScore, boidsEaten);
			PhotonNetwork.room.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() {{playerName, playerData}});
		}
	}

	void PlayerConnected(PhotonPlayer other) {
		players.Add(other);
		addPlayer(other);
	}

	void PlayerDisconnected(PhotonPlayer other) {
		players.Remove(other);
		removePlayer(other);

		if (other.IsMasterClient) {
			var newMaster = players[0];
			foreach (var boid in boidContainer.GetBoids()) boid.GetComponent<PhotonView>().TransferOwnership(newMaster.ID);
		}
	}

	public void DestroyObject(int id) {
		network.RPC("DestroyObjectRPC", PhotonTargets.MasterClient, id);
	}

	[PunRPC]
	void GameStartedRPC(string playerName) {
		chat.SystemLog(string.Format("Gracz {0} rozpoczął grę.", playerName));
		startPlayer(playerName);
	}

	[PunRPC]
	void GameFinishedRPC(string playerName, int boidsEaten) {
		chat.SystemLog(string.Format("Gracz {0} zakończył grę z wynikiem {1}.", playerName, boidsEaten));
		finishPlayer(playerName, boidsEaten);
	}

	[PunRPC]
	void ChatRPC(string playerName, string message) {
		chat.ChatLog(playerName, message);
	}

	public void RPC(string methodName, PhotonTargets target, params object[] parameters) {
		PhotonView.Get(this).RPC(methodName, target, parameters);
	}
}
