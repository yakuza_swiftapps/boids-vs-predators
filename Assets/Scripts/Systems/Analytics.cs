﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Firebase;
using Firebase.Analytics;

public class Analytics : IInitializable {
	[Inject] SignalBus signalBus;

	public void Initialize() {
		signalBus.Subscribe<AnalyticsGameStarted>(() =>
		Firebase.Analytics.FirebaseAnalytics.LogEvent("game_started"));

		signalBus.Subscribe<AnalyticsGameFinished>(x =>
		Firebase.Analytics.FirebaseAnalytics.LogEvent("game_finished", "boids_eaten", x.boidsEaten));
	}
}
