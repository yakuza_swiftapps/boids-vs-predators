﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartBoid : Boid {
	public bool failed;
	public bool panic;

	void Start () {
		base.Start();
	}
	
	void Update () {
		if (!panic) base.Update();

		if (failed) GetComponent<Renderer>().enabled = false;
		else GetComponent<Renderer>().enabled = true;
	}

	public GameObject GetClosestPredator() {
		var minDist = 99999999f;
		GameObject result = null;

		foreach (GameObject predator in predators) {
			var dist = (predator.transform.position - transform.position).sqrMagnitude;

			if (dist < minDist) {
				minDist = dist;
				result = predator;
			}
		}

		return result;
	}

	public int PredatorCount() {return predators.Count;}
}
