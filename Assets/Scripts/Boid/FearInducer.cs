﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FearInducer : MonoBehaviour {
	private Boid boid;

	void Start () {
		boid = GetComponentInParent<Boid>();
	}
	
	void OnTriggerEnter(Collider predator) {
		if (predator.GetComponent<Predator>()) {
			boid.AddPredator(predator.gameObject);
		}
	}
	
	void OnTriggerExit(Collider predator) {
		if (predator.GetComponent<Predator>()) {
			boid.RemovePredator(predator.gameObject);
		}
	}
}
