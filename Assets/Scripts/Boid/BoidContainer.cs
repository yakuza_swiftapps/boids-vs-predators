﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BoidContainer : MonoBehaviour {
	private static BoidContainer currentRoomContainer;

	[Inject] public DiContainer diContainer;

	private List<Transform> boids = new List<Transform>();

	void Awake() {
		currentRoomContainer = this;
	}

	public void Register(Transform boid) {
		boids.RemoveAll((b) => b == null);
		boids.Add(boid);
		diContainer.InjectGameObject(boid.gameObject);
	}

	public List<Transform> GetBoids() {
		return boids;
	}

	public static BoidContainer CurrentRoomContainer() {
		return currentRoomContainer;
	}
}
