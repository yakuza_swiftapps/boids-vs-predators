﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Achievements : IInitializable {
	static Achievements hackinstance;

	[Inject] SignalBus signalBus;
	[Inject] PlayerStats playerStats;

	Dictionary<string, Achievement> achievements = new Dictionary<string, Achievement>();

	public void Initialize () {
		hackinstance = this;

		RegisterAchievement(new Achievement() {id = "eat10", name = "Zjedz 10 boidów", requiredProgress = 10});
		RegisterAchievement(new Achievement() {id = "eat5oneround", name = "Zjedz 5 boidów w jednej rozgrywce", requiredProgress = 5});
		RegisterAchievement(new Achievement() {id = "shootBoid", name = "Zastrzel boida", requiredProgress = 1});
		RegisterAchievement(new Achievement() {id = "3roundswitheat", name = "Rozegraj 3 rozgrywki z rzędu, zjadając co najmniej jednego boida", requiredProgress = 3});
		RegisterAchievement(new Achievement() {id = "popups", name = "Zobacz 10 pop-upów", requiredProgress = 10});

		signalBus.Subscribe<FireBaseInitializedSignal>(LoadAchievements);

		signalBus.Subscribe<AnalyticsGameStarted>(() => {
			achievements["eat5oneround"].ResetProgress();
		});

		signalBus.Subscribe<CreatePopup>(() => {
			achievements["popups"].Progress();
		});

		signalBus.Subscribe<AnalyticsGameFinished>((x) => {
			if (x.boidsEaten == 0) achievements["3roundswitheat"].ResetProgress();
			else achievements["3roundswitheat"].Progress();
		});

		signalBus.Subscribe<BoidEatenSignal>(() => {
			achievements["eat10"].Progress();
			achievements["eat5oneround"].Progress();
		});

		signalBus.Subscribe<BoidShotSignal>(() => {
			achievements["shootBoid"].Progress();
		});
	}

	private void LoadAchievements() { foreach (Achievement ach in achievements.Values) if (playerStats.GetAchievements().Contains(ach.id)) ach.unlocked = true; }

	public void AchievementUnlocked(string name, string id) {
		signalBus.Fire(new AchievementGet() {name = id});
		signalBus.Fire(new CreatePopup() {text = "Osiągnięcie odblokowane: " + name});
	}

	class Achievement {
		public string id;
		public string name;
		public bool unlocked;
		public int requiredProgress;
		private int progress;

		public void Progress(int steps = 1) {
			progress += steps;

			if (!unlocked && progress >= requiredProgress) {
				unlocked = true;
				Achievements.hackinstance.AchievementUnlocked(name, id);
			}
		}

		public void ResetProgress() {progress = 0;}
	}
	
	private void RegisterAchievement(Achievement newAch) {
		achievements[newAch.id] = newAch;
	}
}
