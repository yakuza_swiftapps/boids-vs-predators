﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using Zenject;

public class Boid : MonoBehaviour, IPunObservable {
	protected LayerMask OBSTACLES;
	protected const int RAYCAST_PROBES = 8;
	protected const float RAYCAST_DELTA = Mathf.PI * 2 / RAYCAST_PROBES;

	[Inject] protected Settings settings;
	[Inject] protected HealthBarManager healthBars;

	protected Vector3 velocity;
	protected List<GameObject> neighbors = new List<GameObject>();
	protected List<GameObject> predators = new List<GameObject>();
	protected List<GameObject> walls = new List<GameObject>();
	protected Rigidbody rb;

	protected Vector3 targetPosition;
	protected float lastSync = -1;
	protected float lastSyncDelta;
	protected Vector3 lastPosition;
	protected float smoothFactor = 2f;

	protected void Start () {
		OBSTACLES = LayerMask.GetMask(new string[] {"Obstacle"});

		var boidContainer = BoidContainer.CurrentRoomContainer();
		boidContainer.Register(transform);
		transform.SetParent(boidContainer.transform);
		
		velocity = new Vector3(-1 + Random.value*2, 0, -1 + Random.value*2).normalized * Mathf.Sqrt(settings.MAX_SPEED_SQUARED);
		rb = GetComponent<Rigidbody>();

		healthBars.RegisterBoid(gameObject);
	}
	
	protected void Update () {
		if (!PhotonNetwork.player.IsMasterClient) {
			var t = Mathf.Clamp((Time.time - lastSync) / (Time.time - lastSync + lastSyncDelta) * smoothFactor, 0, 1);
			if (t == 1) targetPosition += (targetPosition - lastPosition) * Time.deltaTime;
			var movePosition = Vector3.Lerp(lastPosition, targetPosition, t);
			if (!float.IsNaN(movePosition.x))transform.position = movePosition;
			// Debug.DrawLine(lastPosition, targetPosition, Color.red, 1, false);
			return;
		}
		Profiler.BeginSample("RemoveAll"); //--------------PROFILER
		neighbors.RemoveAll((boid) => boid == null);
		Profiler.EndSample(); //--------------PROFILER END

		if (!GetComponent<SmartBoid>()) {
			Profiler.BeginSample("Predators"); //--------------PROFILER
			foreach (GameObject predator in predators) {
				AvoidObstacle(predator.transform.position, settings.PREDATOR_AVOID_WEIGHT, false);
			}
			Profiler.EndSample(); //--------------PROFILER END
		}

		Profiler.BeginSample("Neighbors"); //--------------PROFILER
		if (neighbors.Count > 0) {
			Vector3 velocityAverage = Vector3.zero;
			float distanceAverage = 0;

			foreach (GameObject boid in neighbors) {
				velocityAverage += GetVelocity(boid.transform);
				distanceAverage += (boid.transform.position - transform.position).magnitude;
			}

			velocityAverage /= neighbors.Count;
			distanceAverage /= neighbors.Count;

			ModifyVelocity(velocityAverage, velocity, (xz1, xz2) => settings.FLOCK_VELOCITY_ADJUST_WEIGHT * (xz1 - xz2));
			foreach (GameObject boid in neighbors) {
				var dist = (boid.transform.position - transform.position).magnitude;
				ModifyVelocity(boid.transform.position, transform.position, (xz1, xz2) => settings.FLOCK_POSITION_ADJUST_WEIGHT * (xz1 - xz2) * (dist - distanceAverage) / dist);
			}
		}
		Profiler.EndSample(); //--------------PROFILER END

		Profiler.BeginSample("AvoidBoid"); //--------------PROFILER
		foreach (GameObject neighbor in neighbors) { AvoidObstacle(neighbor.transform.position, settings.BOID_AVOID_WEIGHT); }
		Profiler.EndSample(); //--------------PROFILER END

		ModifyVelocity(Vector3.back + Vector3.forward * Random.value * 2, Vector3.left + Vector3.right * Random.value * 2, (xz1, xz2) => settings.VELOCITY_NOISE * (xz1 + xz2));

		Profiler.BeginSample("Math"); //--------------PROFILER
		var speed = (predators.Count > 0 ? settings.ESCAPE_SPEED_SQUARED :  settings.MAX_SPEED_SQUARED);
		if (velocity.sqrMagnitude > 0) transform.rotation = Quaternion.Euler(-90, Vector3.SignedAngle(Vector3.back, velocity.normalized, Vector3.up), 0);
		if (velocity.sqrMagnitude > speed) velocity *= settings.SPEED_DAMPING;
		else if (velocity.sqrMagnitude < speed/2) velocity /= settings.SPEED_DAMPING;
		Profiler.EndSample(); //--------------PROFILER END
	}

	protected void FixedUpdate() {
		Profiler.BeginSample("AvoidWall"); //--------------PROFILER
		if (walls.Count > 0) {
			List<Vector3> obstacles = new List<Vector3>();
			for (int i = 0; i < RAYCAST_PROBES; i++) {
				RaycastHit obstacle;
				if (Physics.Raycast(transform.position, new Vector3(Mathf.Sin(RAYCAST_DELTA * i), 0, Mathf.Cos(RAYCAST_DELTA * i)), out obstacle, settings.RAYCAST_DIST, OBSTACLES)) {
					obstacles.Add(obstacle.point);
				}
			}
			foreach (Vector3 obstacle in obstacles) { AvoidObstacle(obstacle, settings.OBSTACLE_AVOID_WEIGHT); }
		}
		Profiler.EndSample(); //--------------PROFILER END

		Profiler.BeginSample("Move"); //--------------PROFILER
		rb.MovePosition(new Vector3(transform.position.x + velocity.x * Time.deltaTime, 0, transform.position.z + velocity.z * Time.deltaTime));
		Profiler.EndSample(); //--------------PROFILER END
	}

	protected void AvoidObstacle(Vector3 obstacle, float weight, bool distanceLimit = true) {
		var dist = Mathf.Sqrt(Mathf.Pow(obstacle.x - transform.position.x, 2) + Mathf.Pow(obstacle.z - transform.position.z, 2));
		if (distanceLimit && dist > settings.MIN_OBSTACLE_DISTANCE) return;

		ModifyVelocity(obstacle, transform.position, (xz1, xz2) => -(weight * ((xz1 - xz2) * settings.MIN_OBSTACLE_DISTANCE / dist - (xz1 - xz2))));
	}
	
	public void ModifyVelocity(Vector3 modifierVector1, Vector3 modifierVector2, System.Func<float, float, float> modifierFunction) {
		velocity += new Vector3(modifierFunction(modifierVector1.x, modifierVector2.x), 0, modifierFunction(modifierVector1.z, modifierVector2.z));
	}

	public Vector3 GetVelocity() {return velocity;}
	protected Vector3 GetVelocity(Transform otherBoid) {return otherBoid.GetComponent<Boid>().GetVelocity();}

	void OnTriggerEnter(Collider boid) {
		if (boid.GetComponent<Boid>() && boid.gameObject != gameObject) neighbors.Add(boid.gameObject);
	}

	void OnTriggerExit(Collider boid) {
		neighbors.Remove(boid.gameObject);
	}

	public void AddPredator(GameObject predator) {
		predators.Add(predator);
	}

	public void RemovePredator(GameObject predator) {
		predators.Remove(predator);
	}

	public void AddWall(GameObject wall) {
		walls.Add(wall);
	}

	public void RemoveWall(GameObject wall) {
		walls.Remove(wall);
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) {
			stream.SendNext(transform.position.x);
			stream.SendNext(transform.position.z);
			stream.SendNext(transform.rotation.eulerAngles.y);
		} else {
			lastPosition = targetPosition;
			targetPosition = new Vector3((float)stream.ReceiveNext(), 0, (float)stream.ReceiveNext());
			if (settings == null) settings = new Settings(); //HACK :/
			if ((transform.position - targetPosition).sqrMagnitude > settings.MAX_SYNC_DIFF_SQ || lastSync == -1) lastPosition = targetPosition;
			transform.rotation = Quaternion.Euler(-90, (float)stream.ReceiveNext(), 0);
			
			smoothFactor = 1f / ((Time.time - lastSync) / (Time.time - lastSync + lastSyncDelta));
			lastSyncDelta = Time.time - lastSync;
			lastSync = Time.time;
		}
	}
	
	[System.Serializable]
	public class Settings {
		public float RAYCAST_DIST = 5f;

		public float MIN_OBSTACLE_DISTANCE = 4f;
		public float BOID_AVOID_WEIGHT = 0.1f;
		public float PREDATOR_AVOID_WEIGHT = 0.6f;
		public float OBSTACLE_AVOID_WEIGHT = 0.8f;

		public float FLOCK_VELOCITY_ADJUST_WEIGHT = 0.1f;
		public float FLOCK_POSITION_ADJUST_WEIGHT = 0.15f;

		public float VELOCITY_NOISE = 2f;
		public float MAX_SPEED_SQUARED = 400f;
		public float ESCAPE_SPEED_SQUARED = 625f;
		public float SPEED_DAMPING = 0.75f;

		public float MAX_SYNC_DIFF_SQ = 16f;

		public void LoadSettings() {
			MAX_SPEED_SQUARED = Mathf.Pow((float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("boid_normal_speed").DoubleValue, 2);
			ESCAPE_SPEED_SQUARED = Mathf.Pow((float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("boid_escaping_speed").DoubleValue, 2);
			VELOCITY_NOISE = (float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("boid_speed_randomness").DoubleValue;
		}
	}
}