using System.Collections;
using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller<ProjectInstaller>
{
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<BoidEatenSignal>().OptionalSubscriber();
        Container.DeclareSignal<NewGameSignal>().OptionalSubscriber();
        Container.DeclareSignal<AddGameTimeSignal>().OptionalSubscriber();
        Container.DeclareSignal<FireBaseFailSignal>().OptionalSubscriber();
        Container.DeclareSignal<FireBaseNextStepSignal>().OptionalSubscriber();
        Container.DeclareSignal<FireBaseInitializedSignal>().OptionalSubscriber();
        Container.DeclareSignal<IAPInitialized>().OptionalSubscriber();
        Container.DeclareSignal<IAPPurchased>().OptionalSubscriber();
        Container.DeclareSignal<PredatorSpawnedSignal>().OptionalSubscriber();
        Container.DeclareSignal<PhotonInitalizedSignal>().OptionalSubscriber();
        Container.DeclareSignal<AnalyticsGameStarted>().OptionalSubscriber();
        Container.DeclareSignal<AnalyticsGameFinished>().OptionalSubscriber();
        Container.DeclareSignal<CreatePopup>().OptionalSubscriber();
        Container.DeclareSignal<BoidShotSignal>().OptionalSubscriber();
        Container.DeclareSignal<AchievementGet>().OptionalSubscriber();

        Container.BindInterfacesAndSelfTo<PlayerStats>().AsCached();
        Container.BindInterfacesAndSelfTo<Analytics>().AsCached();
        Container.BindInterfacesAndSelfTo<IAPShop>().AsCached();
        Container.BindInterfacesAndSelfTo<Ads>().AsCached();
        Container.BindInterfacesAndSelfTo<Achievements>().AsCached();
        Container.BindInterfacesAndSelfTo<PopupManager>().AsSingle();

        // Container.InstantiatePrefabResource("PopupCanvas");
    }
}

public class AchievementGet {public string name;}
public class BoidEatenSignal {}
public class BoidShotSignal {}
public class NewGameSignal {}
public class AddGameTimeSignal {}
public class FireBaseFailSignal {public int step;}
public class FireBaseNextStepSignal {}
public class FireBaseInitializedSignal {}
public class PhotonInitalizedSignal {}
public class AnalyticsGameStarted {}
public class AnalyticsGameFinished {public int boidsEaten;}
public class IAPInitialized {}
public class IAPPurchased {}
public class PredatorSpawnedSignal {public GameObject predator;}
public class CreatePopup {public string text;}