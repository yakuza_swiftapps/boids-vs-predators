﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using GoogleMobileAds.Api;

public class Lobby : MonoBehaviour {
	[Inject] PlayerStats playerStats;
	[Inject] IAPShop shop;
	[Inject] SignalBus signalBus;

	private const string waitForConnection = "Brak połączenia z siecią. Poczekaj na połączenie.";
	private const string waitForPlayerStats = "Firebase nie zainicjalizowane! Poczekaj.";

	[SerializeField] private NetworkHandler network;
	[SerializeField] private Button createRoomButton;
	[SerializeField] private Button buyCoinsButton;
	[SerializeField] private Button buyCoins2Button;
	[SerializeField] private Button adCoinsButton;
	[SerializeField] private Text errorText;
	[SerializeField] private Text connectedText;
	[SerializeField] private Text coinsText;
	[SerializeField] private Transform roomList;
	[SerializeField] private GameObject noRooms;
	[SerializeField] private Button close;
	[SerializeField] private Button openRoomListButton;
	[SerializeField] private Button refreshButton;
	[SerializeField] private GameObject shopPanel;
	[SerializeField] private Button closeShopButton;

	void Start () {
		ScreenFader.FadeIn();
		PhotonNetwork.JoinLobby();
		PhotonNetwork.playerName = "Gracz" + GenerateRoomName();

		openRoomListButton.onClick.AddListener(openRoomList);
		refreshButton.onClick.AddListener(refreshRooms);
		close.onClick.AddListener(closeRoomList);
		for (int i = 0; i < 5; i++) {
			var button = roomList.GetChild(i);
			int j = i;
			button.GetComponent<Button>().onClick.AddListener(() => roomButtonClick(j));
		}
		createRoomButton.onClick.AddListener(OnCreate);
		coinsText.GetComponent<Button>().onClick.AddListener(openShop);
		closeShopButton.onClick.AddListener(closeShop);
		buyCoinsButton.onClick.AddListener(() => shop.BuyProductID(IAPShop.product1ID));
		buyCoins2Button.onClick.AddListener(() => shop.BuyProductID(IAPShop.product2ID));
		adCoinsButton.onClick.AddListener(ShowAd);

		network.JoinedRoomEvent += JoinedRoom;
		network.PhotonJoinRoomFailedEvent += FailedRoom;
		network.FailedToConnectEvent += NotConnected;
		network.CreateRoomFailedEvent += FailedNewRoom;

		signalBus.Subscribe<IAPPurchased>(updateCoins);
        updateCoins();

		rewardBasedVideo = RewardBasedVideoAd.Instance;
		rewardBasedVideo.OnAdRewarded += rewardAd;
		rewardBasedVideo.OnAdLoaded += ((x, y) => Debug.Log("LOADED"));
		rewardBasedVideo.OnAdFailedToLoad += ((x, y) => Debug.LogError("Rewarded video ad failed to load: " + y.Message));
	}

	RewardBasedVideoAd rewardBasedVideo;

	private void ShowAd() {
		Debug.Log("Add should appear NOW");
        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, "ca-app-pub-3940256099942544/5224354917");
	}

	public void testpopup() {signalBus.Fire(new CreatePopup() {text = "Testowy popup " + Random.value});}

	void Update() {
		if (rewardBasedVideo.IsLoaded())
			rewardBasedVideo.Show();
	}

	private void rewardAd(object sender, Reward args) {
		playerStats.ModifyCoins(10);
		updateCoins();
	}

	private void openRoomList() {
		closeShop();
		roomList.gameObject.SetActive(true);
		refreshRooms();
	}

	private void refreshRooms() {
		var rooms = PhotonNetwork.GetRoomList();
		noRooms.SetActive(rooms.Length == 0);
		for (int i = 0; i < 5; i++) {
			var button = roomList.GetChild(i);
			
			if (i < rooms.Length) {
				button.gameObject.SetActive(true);
				button.GetComponentInChildren<Text>().text = rooms[i].Name + " (" + rooms[i].PlayerCount + "/" + rooms[i].MaxPlayers + " graczy)";
			} else
				button.gameObject.SetActive(false);
		}
	}

	private void closeRoomList() {
		roomList.gameObject.SetActive(false);
	}

	private void openShop() {
		closeRoomList();
		shopPanel.SetActive(true);
		loadProducts();
	}

	private void closeShop() {
		shopPanel.SetActive(false);
	}

	private void roomButtonClick(int id) {
		PhotonNetwork.LeaveLobby();
		var roomName = PhotonNetwork.GetRoomList()[id].Name;
		ScreenFader.FadeOut("Dołączam do pokoju", () => network.JoinRoom(roomName));
	}

	private void JoinedRoom() {
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.LoadLevel("Scenes/SampleScene");
		}
	}

	private void FailedNewRoom(object[] msg) {
		errorText.text = "Nie udało się utworzyć pokoju.";
	}

	private void FailedRoom(object[] msg) {
		if ((short)msg[0] == ErrorCode.GameDoesNotExist) errorText.text = "Podany pokój nie istnieje, więc nie można do niego dołączyć.";
		else errorText.text = "Nie udało się dołączyć, bo tak.";
		connectedText.text = "";
	}

	private void Connected() {
		if (!playerStats.IsInitialized())
			connectedText.text = "Inicjalizuję Firebase...";
		else
			connectedText.text = "";
		if (errorText.text == waitForConnection) errorText.text = "";
	}

	private void NotConnected() {
		connectedText.text = "Nie udało się połączyć z siecią.";
	}

	private void OnCreate() {
		errorText.text = "";
		
		if (PhotonNetwork.connected && playerStats.IsInitialized()) {
			connectedText.text = "Tworzę pokój...";
			ScreenFader.FadeOut("Tworzę pokój", () => network.CreateRoom(GenerateRoomName()));
		} else if (PhotonNetwork.connected)
			errorText.text = waitForPlayerStats;
		else
			errorText.text = waitForConnection;
	}

	private void BuyCoins() {
        if (playerStats.IsInitialized()) shop.BuyProductID(IAPShop.product1ID);
	}

	private void BuyCoins2() {
        if (playerStats.IsInitialized()) shop.BuyProductID(IAPShop.product2ID);
	}

	const string ROOM_NAME_SYMBOLS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	string GenerateRoomName() {
		string name = "";
		for (int i = 0; i < 5; i++) name += ROOM_NAME_SYMBOLS[Random.Range(0, ROOM_NAME_SYMBOLS.Length)];
		return name;
	}

	private void updateCoins() {coinsText.text = ""+playerStats.GetCoins();}

	private void loadProducts() {
		var product = shop.GetProduct(IAPShop.product1ID).metadata;
		buyCoinsButton.GetComponentInChildren<Text>().text = "Kup monety x100\n" + product.localizedPrice + " " + product.isoCurrencyCode;

		product = shop.GetProduct(IAPShop.product2ID).metadata;
		buyCoins2Button.GetComponentInChildren<Text>().text = "Kup monety x151\n" + product.localizedPrice + " " + product.isoCurrencyCode;
	}
}
