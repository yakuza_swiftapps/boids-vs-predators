﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using GoogleMobileAds.Api;

public class Ads : IInitializable {
	private bool loaded;

	public void Initialize () {
		MobileAds.Initialize("ca-app-pub-7478850733296966~1139902410");
		loaded = true;
	}

	public bool IsLoaded() {return loaded;}
}
