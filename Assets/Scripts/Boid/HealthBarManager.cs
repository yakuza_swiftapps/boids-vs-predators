﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarManager : MonoBehaviour {
	Dictionary<GameObject, Image> boids = new Dictionary<GameObject, Image>();

	void Start () {
		
	}
	
	void Update () {
		ArrayList canDelete = new ArrayList();

		foreach (KeyValuePair<GameObject, Image> pair in boids) {
			if (pair.Key == null)
				canDelete.Add(pair.Key);
			else {
				pair.Value.fillAmount = pair.Key.GetComponent<BoidHealth>().GetHealth() / (float)BoidHealth.MAX_HEALTH;
				pair.Value.transform.parent.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(pair.Key.transform.position);
			}
		}

		foreach (GameObject deadBoid in canDelete) {
			Destroy(boids[deadBoid].transform.parent.gameObject);
			boids.Remove(deadBoid);
		}
	}

	public void RegisterBoid(GameObject boid) {
		var image = boid.GetComponentsInChildren<Image>()[1];
		image.transform.parent.SetParent(transform);
		image.transform.parent.GetComponent<RectTransform>().rotation = Quaternion.identity;
		boids[boid] = image;
	}
}
