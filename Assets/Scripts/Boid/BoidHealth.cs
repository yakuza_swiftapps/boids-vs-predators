﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidHealth : MonoBehaviour, IPunObservable {
	public static int MAX_HEALTH = 100;
	private int health = MAX_HEALTH;
	
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) {
			stream.SendNext(health);
		} else {
			health = (int)stream.ReceiveNext();
		}
	}

	public int GetHealth() {return health;}
	public bool Damage(int amount) {
		health -= amount;
		return health <= 0;
	}
}
