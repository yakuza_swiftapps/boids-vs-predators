//
//  TDMediationAdRequest.h
//  Tapdaq
//
//  Created by Dmitry Dovgoshliubnyi on 26/01/2018.
//  Copyright © 2018 Tapdaq. All rights reserved.
//

#import "TDAdRequest.h"

@interface TDMediationAdRequest : TDAdRequest
- (void)display;
@end
