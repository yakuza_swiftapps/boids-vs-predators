﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartPredator : Predator {
	public bool succeeded;
	public bool chasing;

	void Start () {
		base.Start();
	}
	
	void FixedUpdate () {
		if (!chasing) base.Update();
	}

	public GameObject GetClosestBoid() {
		var minDist = 99999999f;
		GameObject result = null;

		foreach (GameObject boid in boids) {
			var dist = (boid.transform.position - transform.position).sqrMagnitude;

			if (dist < minDist) {
				minDist = dist;
				result = boid;
			}
		}

		return result;
	}

	public int BoidCount() {return boids.Count;}
}
