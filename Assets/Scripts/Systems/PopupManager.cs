﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using DG.Tweening;

public class PopupManager {
	[Inject] SignalBus signalBus;

	public RectTransform panel;
	public Text text;
	Sequence animation;

	Queue<string> popups = new Queue<string>();
	
	public void Initialize () {
		signalBus.Subscribe<CreatePopup>((x) => CreatePopup(x.text));

		animation = DOTween.Sequence()
		.Append(panel.DOAnchorPosX(-175.75f, 1))
		.AppendInterval(2)
		.Append(panel.DOAnchorPosX(175.75f, 1))
		.AppendInterval(0.1f)
		.AppendCallback(popPopup)
		.Pause()
		.SetAutoKill(false);
	}

	public void CreatePopup(string _text) {
		popups.Enqueue(_text);
		if (!animation.IsPlaying()) popPopup();
	}

	private void popPopup() {
		if (popups.Count > 0) {
			text.text = popups.Dequeue();
			animation.Restart();
		}
	}
}
