using UnityEngine;
using Zenject;
using MLAgents;

public class GameInstaller : MonoInstaller<GameInstaller>
{
    [SerializeField] private BoidContainer boidContainer;
    [SerializeField] private PredatorSpawner predatorSpawner;
    [SerializeField] private MapSettings mapSettings;
    [SerializeField] private RoomManager roomManager;
    [SerializeField] private NetworkHandler network;
    [SerializeField] private Chat chat;
    [SerializeField] private Scoreboard scoreboard;
    [SerializeField] private HealthBarManager healthBars;
    // [SerializeField] private Brain brain;

    public Boid.Settings BoidSettings = new Boid.Settings();
    public Predator.Settings PredatorSettings = new Predator.Settings();

    public override void InstallBindings() {
        Container.Bind<BoidContainer>().FromInstance(boidContainer);
        Container.Bind<PredatorSpawner>().FromInstance(predatorSpawner);
        Container.Bind<MapSettings>().FromInstance(mapSettings);
        Container.Bind<RoomManager>().FromInstance(roomManager);
        Container.Bind<NetworkHandler>().FromInstance(network);
        Container.Bind<Scoreboard>().FromInstance(scoreboard);
        Container.Bind<Chat>().FromInstance(chat);
        Container.Bind<HealthBarManager>().FromInstance(healthBars);
        // Container.Bind<Brain>().FromInstance(brain);

        BoidSettings.LoadSettings();
        Container.BindInstance(BoidSettings);
        PredatorSettings.LoadSettings();
        Container.BindInstance(PredatorSettings);
    }
}