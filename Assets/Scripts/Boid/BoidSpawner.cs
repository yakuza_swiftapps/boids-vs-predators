﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BoidSpawner : MonoBehaviour {
	[Inject] private MapSettings mapSettings;
	[Inject] private PredatorSpawner predatorSpawner;
	[Inject] private BoidContainer boidContainer;

	LayerMask FLOOR;
	Vector4 bounds;

	[SerializeField] private int MAX_SPAWN_ITERATIONS = 10000;
	[SerializeField] private float ABOVE_FLOOR = 0.6f;
	[SerializeField] private float MAX_SPAWN_CLICK_LENGTH = 0.05f;

	private float clickTimer;
	private int maxBoids = 200;

	void Start() {
		FLOOR = LayerMask.GetMask(new string[] {"Floor"});
		bounds = mapSettings.GetBounds();

		maxBoids = (int)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("max_boids").LongValue;
	}

	void Update () {
		if (predatorSpawner.IsPredatorActive()) return;
		
		if (Input.GetKey(KeyCode.Space)) {
			for (int i = 0; i < MAX_SPAWN_ITERATIONS; i++) {
				var pos = new Vector2(bounds.x + Random.Range(-bounds.z/2, bounds.z/2), bounds.y + Random.Range(-bounds.w/2, bounds.w/2));
				if (TrySpawn(new Vector3(pos.x, Camera.main.transform.position.y, pos.y))) break;
			}
		}
		
		if (Input.GetMouseButtonDown(0)) clickTimer = Time.time;
		else if (Input.GetMouseButtonUp(0) && Time.time - clickTimer <= MAX_SPAWN_CLICK_LENGTH) {
			var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, Camera.main.transform.position.y));
			TrySpawn(new Vector3(pos.x, Camera.main.transform.position.y, pos.z));
		}
	}

	RaycastHit CheckFloor(Vector3 raycastOrigin) {
		RaycastHit floor;
		Physics.Raycast(raycastOrigin, Vector3.down, out floor, Camera.main.transform.position.y * 2, FLOOR); //TODO: uważać na ściany (te wewnętrzne)
		return floor;
	}

	bool TrySpawn(Vector3 where) {
		if (boidContainer.GetBoids().Count >= maxBoids) return true;

		var floor = CheckFloor(where);
		if (floor.collider != null) {
			PhotonView.Get(this).RPC("SpawnBoid", PhotonTargets.MasterClient, new Vector3(where.x, floor.point.y + ABOVE_FLOOR, where.z));

			return true;
		}

		return false;
	}

	[PunRPC]
	void SpawnBoid(Vector3 where) {
		PhotonNetwork.Instantiate("Boid", where, Quaternion.Euler(-90, 0 ,0 ), 0);
	}
}
