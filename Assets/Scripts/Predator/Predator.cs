﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Predator : Photon.PunBehaviour, IPunObservable {
	[Inject] protected Settings settings;
	[Inject] protected SignalBus signalBus;

	public bool playerControlled = false;
	protected float lifeTime = 0;
	protected Vector3 velocity;
	protected List<GameObject> boids = new List<GameObject>();
	protected Rigidbody rb;
	protected PredatorSpawner predatorSpawner;

	protected SingleJoystick joystick;
	protected Text UILifetime;
	protected Text UIScore;
	protected Text nameLabel;

	protected Vector3 targetPosition;
	protected float lastSync;
	protected float lastSyncDelta;
	protected Vector3 lastPosition;

	protected int score;

	protected void Awake() {
		nameLabel = GetComponentInChildren<Text>();
	}

	protected void Start () {
		if (playerControlled) {
			predatorSpawner = transform.parent.GetComponent<PredatorSpawner>();
			
			joystick = predatorSpawner.GetJoystick();
			UILifetime = predatorSpawner.GetLifetimeLabel();
			UIScore = predatorSpawner.GetConsumedLabel();
			
			lifeTime = settings.CONTROL_TIME_SECONDS;
		} else if (isAI) {
			velocity = new Vector3(-1 + Random.value*2, 0, -1 + Random.value*2).normalized * Mathf.Sqrt(settings.MAX_SPEED_SQUARED);
		}

		rb = GetComponent<Rigidbody>();
	}

	override public void OnPhotonInstantiate(PhotonMessageInfo info) {
		nameLabel.text = info.sender.NickName;
	}
	
	protected void Update () {
		if (isAI && !playerControlled) {
			DoAI();
			return;
		}
		boids.RemoveAll((boid) => boid == null);

		nameLabel.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(transform.position);

		if (playerControlled) {
			var input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			input += new Vector3(joystick.GetInputDirection().x, 0, joystick.GetInputDirection().y);
			velocity += input * settings.PLAYER_ACCELERATION;

			UILifetime.text = string.Format("Pozostały czas: {0:0.00}", lifeTime);
			UIScore.text = string.Format("Zjedzone boidy: {0}", score);

			if (!isAI) {
				lifeTime -= Time.deltaTime;
				if (lifeTime <= 0) {
					UILifetime.text = "Koniec czasu!";
					predatorSpawner.FreePredator(this);
				}
			}

			if (Input.GetKeyDown(KeyCode.Z)) {
				var bullet = PhotonNetwork.Instantiate("PredatorBullet", transform.position - transform.up * settings.BULLET_HOTSPOT, Quaternion.LookRotation(-transform.up, Vector3.down), 0);
				bullet.GetComponentInChildren<BoidDamager>().SetPredator(this);
			}
		} else {
			transform.position = Vector3.Lerp(lastPosition, targetPosition, (Time.time - lastSync) / (Time.time - lastSync + lastSyncDelta));
			return;
		}
	}

	void FixedUpdate() {
		var speed = (boids.Count > 0 ? settings.CHASING_SPEED_SQUARED :  settings.MAX_SPEED_SQUARED);
		if (velocity.sqrMagnitude > 0) transform.rotation = Quaternion.Euler(-90, Vector3.SignedAngle(Vector3.forward, velocity.normalized, Vector3.up), 0);
		if (velocity.sqrMagnitude > speed) velocity *= settings.SPEED_DAMPING;
		else if (velocity.sqrMagnitude < speed/2) velocity /= settings.SPEED_DAMPING;

		var movementVector = new Vector3(velocity.x * Time.deltaTime, 0, velocity.z * Time.deltaTime);
		rb.MovePosition(new Vector3(transform.position.x, 0, transform.position.z) + movementVector);
	}
	////////////-------------------------------------------------AI
	public bool isAI = false;
	protected LayerMask OBSTACLES;
	protected const int RAYCAST_PROBES = 8;
	protected const float RAYCAST_DELTA = Mathf.PI * 2 / RAYCAST_PROBES;

	public float MIN_OBSTACLE_DISTANCE = 4f;
	public float BOID_AVOID_WEIGHT = 0.1f;
	public float PREDATOR_AVOID_WEIGHT = 0.6f;
	public float OBSTACLE_AVOID_WEIGHT = 0.8f;

	void DoAI() {
		boids.RemoveAll((boid) => boid == null);

		List<Vector3> obstacles = new List<Vector3>();
		for (int i = 0; i < RAYCAST_PROBES; i++) {
			RaycastHit obstacle;
			if (Physics.Raycast(transform.position, new Vector3(Mathf.Sin(RAYCAST_DELTA * i), 0, Mathf.Cos(RAYCAST_DELTA * i)), out obstacle, 5f, OBSTACLES)) {
				obstacles.Add(obstacle.point);
			}
		}
		foreach (Vector3 obstacle in obstacles) { AvoidObstacle(obstacle, OBSTACLE_AVOID_WEIGHT); }

		var dist = Mathf.Infinity;
		GameObject currentTarget = null;

		foreach (GameObject boid in boids) {
			var dist2 = (transform.position - boid.transform.position).sqrMagnitude;

			if (dist2 < dist) {
				dist = dist2;
				currentTarget = boid;
			}
		}

		if (currentTarget != null) {
			ModifyVelocity(currentTarget.transform.position, transform.position, (xz1, xz2) => 1f * (xz1 - xz2));
		}

		ModifyVelocity(Vector3.back + Vector3.forward * Random.value * 2, Vector3.left + Vector3.right * Random.value * 2, (xz1, xz2) => 1f * (xz1 + xz2));

		var speed = (boids.Count > 0 ? settings.CHASING_SPEED_SQUARED :  settings.MAX_SPEED_SQUARED);
		if (velocity.sqrMagnitude > 0) transform.rotation = Quaternion.Euler(-90, Vector3.SignedAngle(Vector3.forward, velocity.normalized, Vector3.up), 0);
		if (velocity.sqrMagnitude > speed) velocity *= settings.SPEED_DAMPING;
		else if (velocity.sqrMagnitude < speed/2) velocity /= settings.SPEED_DAMPING;

		rb.MovePosition(new Vector3(transform.position.x + velocity.x * Time.deltaTime, 0, transform.position.z + velocity.z * Time.deltaTime));
	}

	protected void AvoidObstacle(Vector3 obstacle, float weight, bool distanceLimit = true) {
		var dist = Mathf.Sqrt(Mathf.Pow(obstacle.x - transform.position.x, 2) + Mathf.Pow(obstacle.z - transform.position.z, 2));
		if (distanceLimit && dist > MIN_OBSTACLE_DISTANCE) return;

		ModifyVelocity(obstacle, transform.position, (xz1, xz2) => -(weight * ((xz1 - xz2) * MIN_OBSTACLE_DISTANCE / dist - (xz1 - xz2))));
	}
////////-----------------------------------------------------------------/AI
	void OnTriggerEnter(Collider boid) {
		if (boid.GetComponent<Boid>() && boid.gameObject != gameObject) boids.Add(boid.gameObject);
	}

	void OnTriggerExit(Collider boid) {
		boids.Remove(boid.gameObject);
	}
	
	public void ModifyVelocity(Vector3 modifierVector1, Vector3 modifierVector2, System.Func<float, float, float> modifierFunction) {
		velocity += new Vector3(modifierFunction(modifierVector1.x, modifierVector2.x), 0, modifierFunction(modifierVector1.z, modifierVector2.z));
	}

	public virtual void EatBoid(GameObject boid, bool shot = false) {
		if (playerControlled) {
			predatorSpawner.EatBoid(boid.GetComponent<Boid>(), shot);
			score++;
		}
	}

	public int GetBoidsEaten() {return score;}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
		if (stream.isWriting) {
			stream.SendNext(transform.position.x);
			stream.SendNext(transform.position.z);
			stream.SendNext(transform.rotation.eulerAngles.y);
		} else {
			lastSyncDelta = Time.time - lastSync;
			lastSync = Time.time;

			targetPosition = new Vector3((float)stream.ReceiveNext(), 0, (float)stream.ReceiveNext());
			if ((transform.position - targetPosition).sqrMagnitude > 16) transform.position = targetPosition;
			lastPosition = transform.position;
			transform.rotation = Quaternion.Euler(-90, (float)stream.ReceiveNext(), 0);
		}
	}
	
	[System.Serializable]
	public class Settings {
		public float MAX_SPEED_SQUARED = 225;
		public float CHASING_SPEED_SQUARED = 625f;
		public float SPEED_DAMPING = 0.75f;

		public float PLAYER_ACCELERATION = 3f;
		public float CAMERA_AHEAD = 200f;
		public float CONTROL_TIME_SECONDS = 60f;

		public float BULLET_HOTSPOT = 5f;

		public void LoadSettings() {
			CONTROL_TIME_SECONDS = (float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("predator_control_time").DoubleValue;
			MAX_SPEED_SQUARED = Mathf.Pow((float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("predator_normal_speed").DoubleValue, 2);
			CHASING_SPEED_SQUARED = Mathf.Pow((float)Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("predator_chasing_speed").DoubleValue, 2);
		}
	}
}
