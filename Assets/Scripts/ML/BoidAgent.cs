﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using Zenject;

public class BoidAgent : Agent {
	[Inject] private MapSettings mapSettings;
  // [Inject] public Brain brain;

	LayerMask FLOOR;
	private float ABOVE_FLOOR = 0.6f;
	private int MAX_SPAWN_ITERATIONS = 100;
  private Vector4 bounds;
  private float previousDist;

  SmartBoid boid;

  void Start () {
		FLOOR = LayerMask.GetMask(new string[] {"Floor"});
    boid = GetComponent<SmartBoid>();
    bounds = mapSettings.GetBounds();
  }

  public override void AgentReset() {
    for (int i = 0; i < MAX_SPAWN_ITERATIONS; i++) {
      var pos = new Vector3(bounds.x + Random.Range(-bounds.z/2, bounds.z/2), bounds.y + Random.Range(-bounds.w/2, bounds.w/2), 0);
      pos = new Vector3(pos.x, Camera.main.transform.position.y, pos.y);

      var floor = CheckFloor(pos);
      if (floor.collider != null) {
        transform.position = new Vector3(pos.x, floor.point.y + ABOVE_FLOOR, pos.z);
        break;
       }
    }

    boid.failed = false;
	}

	RaycastHit CheckFloor(Vector3 raycastOrigin) {
		RaycastHit floor;
		Physics.Raycast(raycastOrigin, Vector3.down, out floor, Camera.main.transform.position.y * 2, FLOOR); //TODO: uważać na ściany (te wewnętrzne)
		return floor;
	}

	public override void CollectObservations() {
    var predator = boid.GetClosestPredator();
    AddVectorObs(boid.PredatorCount());

    if (predator) {
      boid.panic = true;
      var relativePosition = predator.transform.position - transform.position;
      AddVectorObs(relativePosition.x / bounds.z);
      AddVectorObs(relativePosition.z / bounds.w);
      AddVectorObs(predator.transform.rotation.z / Mathf.PI);
    } else {
      boid.panic = false;
      AddVectorObs(0);
      AddVectorObs(0);
      AddVectorObs(0);
    }
	}
	
	public override void AgentAction(float[] vectorAction, string textAction) {
		boid.ModifyVelocity(new Vector3(vectorAction[0] * 10f, 0, 0), new Vector3(0, 0, vectorAction[1] * 10f), (x, y) => x + y);

    if (boid.failed) {
      AddReward(-1f);
      Done();
    } else {
      var predator = boid.GetClosestPredator();

      if (predator) {
        var dist = (predator.transform.position - transform.position).sqrMagnitude;
        if (dist > previousDist) AddReward(0.1f);
        previousDist = dist;
      } else {
        // AddReward(0.01f);
      }
    }
	}
}
