﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSettings : MonoBehaviour {
	[SerializeField] private float cameraHeight = 40;

	void Update() {
		Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, cameraHeight, Camera.main.transform.position.z);
	}

	public Vector4 GetBounds() {
		var bounds = GetComponent<BoxCollider>();
		if (bounds) {
			return new Vector4(bounds.center.x, bounds.center.z, bounds.size.x, bounds.size.z);
		} else {
			Debug.LogError("Obiekt ten musi posiadać BoxCollider, który określa wymiary płaszczyzny mapy");
			return Vector4.zero;
		}
	}
}
