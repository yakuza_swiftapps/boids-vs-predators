﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Zenject;

public class Loading : MonoBehaviour {
    [Inject] private PlayerStats playerStats;
    [Inject] private SignalBus signalBus;
    [Inject] private IAPShop shop;
    [Inject] private Ads ads;

    [SerializeField] private RectTransform rotator;
    [SerializeField] private Text errorText;
    [SerializeField] private Text photon;
    [SerializeField] private Text firebase;
    [SerializeField] private Image progressBarImage;

    private const float LOAD_SEGMENTATION = 1000;
    private const float MAX_PROGRESS = 7 * LOAD_SEGMENTATION;

    private float barProgress;

    void Awake() {
        PhotonNetwork.autoJoinLobby = false;
        PhotonNetwork.automaticallySyncScene = true;
        PhotonNetwork.autoCleanUpPlayerObjects = false;
    }

    void Start() {
        signalBus.Subscribe<FireBaseNextStepSignal>(() => errorText.text = "");
        signalBus.Subscribe<FireBaseFailSignal>((x) => errorText.text = string.Format("Błąd Firebase na kroku {0} z 5.", x.step));
        signalBus.Subscribe<FireBaseInitializedSignal>(() => firebase.text = "");
    }
	
	void Update () {
        if (barProgress >= MAX_PROGRESS) {
			ScreenFader.FadeOut("Wchodzę do lobby", () => SceneManager.LoadScene("Scenes/Lobby"));
        }
        if (PhotonNetwork.connected) photon.text = "";

        if (barProgress < Progress() * LOAD_SEGMENTATION) barProgress += (Progress() * LOAD_SEGMENTATION - barProgress)/3+1;
        else barProgress += 1;

        rotator.RotateAround(rotator.position, Vector3.forward, 3f);

        progressBarImage.fillAmount = (barProgress / MAX_PROGRESS);
	}

    private float Progress() {
        return playerStats.LoadingProgress() + (PhotonNetwork.connected ? 1 : 0) + (ads.IsLoaded() ? 1 : 0);// + (shop.IsInitialized() ? 1 : 0);
    }
}
