﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class PredatorSpawner : MonoBehaviour {
	[Inject] private RoomManager currentRoom;
	[Inject] private SignalBus signalBus;
	[Inject] private DiContainer diContainer;

	[SerializeField] private GameObject joystick;
	[SerializeField] private GameObject lifetimeLabel;
	[SerializeField] private GameObject consumedLabel;
	[SerializeField] private GameObject spawnButton;

	void Update() {
		if (IsPredatorActive()) signalBus.Fire(new AddGameTimeSignal());
	}

	public void TrySpawnPredator() {
		if (!IsPredatorActive()) SpawnPredator();
	}

	void SpawnPredator() {
		var _predator = PhotonNetwork.Instantiate("Predator", transform.position, Quaternion.Euler(-90, 0 ,0 ), 0);
		_predator.GetComponent<Predator>().playerControlled = true;
		_predator.transform.SetParent(transform);
		diContainer.InjectGameObject(_predator);
		signalBus.Fire(new PredatorSpawnedSignal() {predator = _predator});

		spawnButton.SetActive(false);
		joystick.SetActive(true);
		lifetimeLabel.SetActive(true);
		consumedLabel.SetActive(true);

		signalBus.Fire(new NewGameSignal());
		signalBus.Fire(new AnalyticsGameStarted());
	}

	public void FreePredator(Predator predator) {
		spawnButton.SetActive(true);
		joystick.SetActive(false);
		signalBus.Fire(new AnalyticsGameFinished() {boidsEaten = predator.GetBoidsEaten()});
		PhotonNetwork.Destroy(PhotonView.Get(predator));
	}

	public void EatBoid(Boid boid, bool shot) {
		signalBus.Fire(new BoidEatenSignal());
		if (shot) signalBus.Fire(new BoidShotSignal());
		currentRoom.DestroyObject(PhotonView.Get(boid).viewID);
	}

	public SingleJoystick GetJoystick() {return joystick.GetComponent<SingleJoystick>();}
	public Text GetLifetimeLabel() {return lifetimeLabel.GetComponent<Text>();}
	public Text GetConsumedLabel() {return consumedLabel.GetComponent<Text>();}

	public bool IsPredatorActive() {return joystick.activeSelf;}
}
