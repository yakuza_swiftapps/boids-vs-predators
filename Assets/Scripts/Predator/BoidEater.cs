﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidEater : MonoBehaviour {
	private Predator predator;

	void Start () {
		predator = GetComponentInParent<Predator>();
	}
	
	void OnTriggerEnter(Collider boid) {
		if (boid.GetComponent<SmartBoid>()) {
			boid.GetComponent<SmartBoid>().failed = true;
			if (transform.parent.GetComponent<SmartPredator>() != null) {
				
				transform.parent.GetComponent<SmartPredator>().succeeded = true;
			}
		} else if (boid.GetComponent<Boid>()) {
			predator.EatBoid(boid.gameObject);
		}
		
	}
}
