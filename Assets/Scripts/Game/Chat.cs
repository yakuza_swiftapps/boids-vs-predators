﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Chat : MonoBehaviour {
	[Inject] Chat chat;
	[Inject] SignalBus signalBus;
	[Inject] NetworkHandler network;

	[SerializeField] private int MAX_LINES = 6;

	List<string> messages = new List<string>();
	Text texts;
	
	void Start () {
		texts = GetComponent<Text>();
		updateTexts();

		signalBus.Subscribe<AnalyticsGameStarted>(() => SystemLog("Rozpoczęto grę."));
		signalBus.Subscribe<AnalyticsGameFinished>((x) => SystemLog(string.Format("Koniec gry. Udało się zjeść {0} boidów.", x.boidsEaten)));

		network.PlayerConnectedEvent += (x) => SystemLog("Gracz " + x.NickName + " dołącza do gry.");
		network.PlayerDisconnectedEvent += (x) => SystemLog("Gracz " + x.NickName + " odchodzi.");
	}
	
	void Update () {
		
	}

	public void SystemLog(string message) {
		messages.Insert(0, message);
		updateTexts();
	}

	public void ChatLog(string player, string message) {
		messages.Insert(0, player + ": " + message);
		updateTexts();
	}

	private void updateTexts() {
		var builder = "";
		for (int i = Mathf.Min(messages.Count-1, MAX_LINES-1); i >= 0; i--) {
			builder += messages[i];
			if (i > 0) builder += "\n";
		}

		texts.text = builder;
	}
}
