﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenFader : MonoBehaviour {
	private static Image fade;
	private static Text loadingText;
	private static bool fading;

	void Awake() {
		DontDestroyOnLoad(transform.parent.gameObject);
	}

	void Start () {
		fade = GetComponent<Image>();
		loadingText = GetComponentInChildren<Text>();
	}
	
	public static void FadeOut(string text, Action afterFade) {
		if (fading) return;
		fading = true;
		loadingText.text = text;

		DOTween.Sequence()
		.Append(fade.DOFade(1, 0.5f))
		.AppendCallback(() => loadingText.enabled = true)
		.AppendCallback(() => afterFade())
		.AppendCallback(() => fading = false);
	}
	
	public static void FadeIn() {
		if (loadingText == null) return;
		loadingText.enabled = false;
		fade.DOFade(0, 0.5f);
	}
}
