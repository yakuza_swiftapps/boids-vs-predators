﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidDamager : MonoBehaviour {
	private Predator predator;

	public void SetPredator(Predator _predator) {predator = _predator;}

	void OnTriggerEnter(Collider boid) {
		if (boid.GetComponent<BoidHealth>()) {
			if (boid.GetComponent<BoidHealth>().Damage(20)) {
				predator.EatBoid(boid.gameObject, true);
			}

			PhotonNetwork.Destroy(PhotonView.Get(transform.parent));
		}
	}
}
