#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("T1OqtfyL6MFsY4rpcSA33PO5gWGBEBYXiIBAEEfTYACZvGiDQiZgDgNoMoLwE+TdYuF/5K8UvvzxS6jZW0nsjTbo2IMURaPw30SkCSNS7GB0euoI9wbBNqtMdHv+wyC/nVFxTS/vJuKQq/5N1hjiR3Khkkv+E30oXpqgCorC98qnGw8QpVh/cD/jZQbI9TPnhJxKkOrIf5dzUiBJwqvoNL3rvr5tKTdWoB64I76aFtiNhQzOOUKJx8Gi6+v6WKoDXL3nLb95o383vsVsAXVMUwnEO/WKTKgE4k8IkG7cX3xuU1hXdNgW2KlTX19fW15d3BMSI7SYdlxr9nJtzUP3QlAYuTTcX1FebtxfVFzcX19e8/v+aBi7stfX83cRRydmu1xdX15f");
        private static int[] order = new int[] { 12,6,5,13,11,13,11,13,13,10,13,12,13,13,14 };
        private static int key = 94;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
