﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Cinemachine;

public class CameraManagement : MonoBehaviour {
	[Inject] SignalBus signalBus;
	
	[SerializeField] private GameObject dragMover;

	private enum Cameras{FREE_CAMERA, PREDATOR_CAMERA};
	private CinemachineVirtualCamera[] vCams;

	void Start () {
		vCams = GetComponentsInChildren<CinemachineVirtualCamera>();

		signalBus.Subscribe<PredatorSpawnedSignal>(x => {
			GetCamera(Cameras.PREDATOR_CAMERA).Follow = GetCamera(Cameras.PREDATOR_CAMERA).LookAt = x.predator.transform;
			SwitchToCamera(Cameras.PREDATOR_CAMERA);
		});
		signalBus.Subscribe<AnalyticsGameFinished>(() => SwitchToCamera(Cameras.FREE_CAMERA));

		SwitchToCamera(Cameras.FREE_CAMERA);
	}
	
	void Update () {
		
	}

	private void SwitchToCamera(Cameras id) {
		foreach (Cameras cam in Enum.GetValues(typeof(Cameras))) GetCamera(cam).enabled = (id == cam);
	}

	private CinemachineVirtualCamera GetCamera(Cameras id) {
		return vCams[(int)id];
	}
}
