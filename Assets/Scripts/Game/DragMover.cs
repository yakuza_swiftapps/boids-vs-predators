﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class DragMover : MonoBehaviour {
	[Inject] SignalBus signalBus;

	[SerializeField] private float MIN_MOVEMENT_DRAG_TIME = 0.08f;
	[SerializeField] private float DRAG_FACTOR = 0.1f;

	private Vector4 dragPoint;
	private float dragTimer;
	private GameObject currentPredator;

	void Start () {
		signalBus.Subscribe<PredatorSpawnedSignal>(x => currentPredator = x.predator);
		signalBus.Subscribe<AnalyticsGameFinished>(() => {
			transform.position = currentPredator.transform.position;
			currentPredator = null;
		});
	}
	
	void Update () {
		if (currentPredator == null) {
			if (Input.GetMouseButtonDown(0)) {
				dragPoint = new Vector4(Input.mousePosition.x, Input.mousePosition.y, transform.position.x, transform.position.z);
				dragTimer = Time.time;
			} else if (Input.GetMouseButton(0) && Time.time - dragTimer >= MIN_MOVEMENT_DRAG_TIME) {
				transform.position = new Vector3(dragPoint.z, transform.position.y, dragPoint.w) +
				new Vector3(dragPoint.x - Input.mousePosition.x, 0, dragPoint.y - Input.mousePosition.y) * DRAG_FACTOR;
			}
		}
	}
}
