﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FPSMeter : MonoBehaviour {
	[Inject] BoidContainer boidContainer;

	float deltaTime = 0.0f;
 
	void Update() {
		deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		GetComponent<UnityEngine.UI.Text>().text = string.Format("{0:0.0} ms ({1:0.} fps) / {2:0} boidów", msec, fps, boidContainer.GetBoids().Count);
	}
}
